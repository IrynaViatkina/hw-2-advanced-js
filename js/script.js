class Hamburger {
	constructor(size, stuffing) {
		try{
			if(arguments.length < 2){
				throw new HamburgerException('Missing required data!');
			}
			if(!Hamburger.sizes[size]){
				throw new HamburgerException(`Invalid size ${size}`);
			}	
			if(!Hamburger.stuffings[stuffing]){
				throw new HamburgerException(`Invalid stuffing ${stuffing}`);
			}
			else {
				this.stuffing = stuffing;
				this.size = size;
				this.toppingArray = [];
			}
		}
		catch(e){
			console.log(e.message);
		}
	}
	get sizeOfHamburger() {
		return this.size;
	}
	get stuffingOfHamburger() {
		return this.stuffing;
	}
	get toppings() {
		return this.toppingArray;
	}
	addTopping(topping){
		try{
			if(!this.toppingArray.includes(topping)){
				this.toppingArray.push(topping);
			}
			else{
				throw new HamburgerException(`Duplicate topping: ${topping}`);
			}
		}
		catch(e){
			console.log(e.message);
		}
	}
	removeTopping (topping){
		let toppingIndex = this.toppingArray.indexOf(topping);
		try{
			if(~toppingIndex){
				this.toppingArray.splice(toppingIndex, 1);
			}
			else{
				throw new HamburgerException(`You do not have this topping: ${topping}!`);
			}
		}
		catch(e){
			console.log(e.message);
		}
	}
	calculateCalories(){
		return Hamburger.sizes[this.sizeOfHamburger].calories + Hamburger.stuffings[this.stuffingOfHamburger].calories + this.toppings.reduce(function(accum, current){
			return accum + Hamburger.toppings[current].calories;
		}, 0);
	}
	calculatePrice (){
		return Hamburger.sizes[this.sizeOfHamburger].price + Hamburger.stuffings[this.stuffingOfHamburger].price + this.toppings.reduce(function(accum, current){
			return accum + Hamburger.toppings[current].price;
		}, 0);
	}
} 

Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';
Hamburger.sizes = {
	[Hamburger.SIZE_LARGE]: {
		price: 100,
		calories: 40
	},
	[Hamburger.SIZE_SMALL]: {
		price: 50,
		calories: 20
	}
};

Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';
Hamburger.stuffings = {
	[Hamburger.STUFFING_CHEESE]: {
		price: 10,
		calories: 20
	},
	[Hamburger.STUFFING_SALAD]: {
		price: 20,
		calories: 5
	},
	[Hamburger.STUFFING_POTATO]: {
		price: 15,
		calories: 10
	}
};
Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.toppings = {
	[Hamburger.TOPPING_MAYO]: {
		price: 20,
		calories: 5
	},
	[Hamburger.TOPPING_SPICE]: {
		price: 15,
		calories: 0
	}
};

class HamburgerException extends Error{
	constructor(message){
		super(message);
	}
}

/*for tests
const ham = new Hamburger();
console.log(ham);
const h = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
console.log(h);
h.addTopping(Hamburger.TOPPING_MAYO);
h.addTopping(Hamburger.TOPPING_SPICE);
h.addTopping(Hamburger.TOPPING_SPICE);
h.removeTopping(Hamburger.TOPPING_SPICE);
h.removeTopping(Hamburger.TOPPING_SPICE);
h.addTopping(Hamburger.TOPPING_MAYO);
console.log(h.calculateCalories());
console.log(h.calculatePrice());
console.log(h.toppings);
console.log(h.size === Hamburger.SIZE_SMALL);
console.log(h.stuffing);
*/
